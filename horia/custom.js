//copied from http://www.chrisbuttery.com/articles/fade-in-fade-out-with-javascript/

// fade out
function fadeOut(el, step){
  step = step || 10;
  step = 1 / step;
  el.style.opacity = 1;

  (function fade() {
    if ((el.style.opacity -= step) < 0) {
      el.style.display = "none";
    } else {
      requestAnimationFrame(fade);
    }
  })();
}

// fade in
function fadeIn(el, step, display){
  step = step || 10;
  step = 1 / step;
  el.style.opacity = 0;
  el.style.display = display || "block";

  (function fade() {
    var val = parseFloat(el.style.opacity);
    if (!((val += step) > 1)) {
      el.style.opacity = val;
      requestAnimationFrame(fade);
    }
  })();
}


document.addEventListener("DOMContentLoaded", function(event) { 
	var pencil = $("#pencil");
	var pencil_broken = $("#pencil_broken");
	var container = $(".container");
	var illustrations = $(".illustrations").find("img");

	container.hover(
		//hover on
		function() {
			pencil.hide();
			pencil_broken.show();
			//http://api.jquery.com/fadeIn/
			illustrations.fadeIn( {
				queue: false, 
				duration: 'fast'
			});
			illustrations.animate({
		    	width: "115px",
			}, "fast" );
		},  
		//hover off
		function() {
			pencil_broken.hide();
			pencil.show();
			illustrations.animate({
		    	width: "0",
			}, "fast" );
			illustrations.hide();
		}
	);

	$('#visualsicon').on('mouseenter', function() {
		$(this).attr('src', 'images/visualsicon2.png');
	});

	$('#visualsicon').on('mouseleave', function() {
		$(this).attr('src', 'images/visualsicon.png');
	});

	$('#lighticon').on('mouseenter', function() {
		$(this).attr('src', 'images/lighticon2.png');
	});
	
	$('#lighticon').on('mouseleave', function() {
		$(this).attr('src', 'images/lighticon.png');
	});
	$('#eyeicon').on('mouseenter', function() {
		$(this).attr('src', 'images/eyeicon2.png');
	});
	
	$('#eyeicon').on('mouseleave', function() {
		$(this).attr('src', 'images/eyeicon.png');
	});


//hide/show with js
	// var pencil = document.getElementById("pencil");
	// var pencil2 = document.getElementById("pencil2");
	// pencil.addEventListener("mouseover", function() {
	// 	pencil.style.display = "none";
	// 	pencil2.style.display = "block";
	// 	fade
	// });  
	// pencil2.addEventListener("mouseout", function() {
	// 	pencil.style.display = "block";
	// 	pencil2.style.display = "none";
	// });  
});